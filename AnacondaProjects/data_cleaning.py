import pandas as pd

data=pd.read_csv('forestfires.csv')
data=data.drop('day', axis=1)
data=data[data.area < 500]
print(data)
data.to_csv('clean_data.csv', sep=',')