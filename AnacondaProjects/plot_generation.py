from matplotlib import pyplot as plt
import pandas as pd

def write_correlation():
    """ writes correlaions to a file """
    with open("correaltion matrix.txt", 'w') as f:
        f.write(str(data.corr()))


def histograms(data):
    """  plots out histograms for all numeric data and ignores nominal data  """
    for name, values in data.iteritems():
        try:
            plt.hist(values)
            plt.title("histogram for {0}".format(name))
            plt.xlabel(name)
            plt.ylabel("frequency")
            plt.autoscale()
            plt.show()
            plt.clf()
        except TypeError:
            pass
def bar_plots(data):
    mcnt = []
    for i in sorted(set(data.month)):
        x = data[data.month == i]
        mcnt.append(len(x.index))
    plt.bar(range(10), mcnt)
    plt.title("number of fires in months")
    plt.autoscale()
    print(set(data.month))
    plt.xticks(range(10), sorted(set(data.month)))
    plt.show()
    plt.legend("fires in august and july")
    plt.clf()
    wcnt = []
    for i in sorted(set(data.day)):
        x = data[data.day == i]
        wcnt.append(len(x.index))
    plt.bar(range(7), wcnt)
    plt.title("number of fires in day")
    plt.autoscale()
    print(set(data.day))
    plt.xticks(range(7), sorted(set(data.day)))
    plt.show()
    plt.legend("fires on days")
    plt.clf()

def plots_vs_area(data):
    for name, values in data.iteritems():
        if not name in ['month', 'day', 'area']:
            plt.scatter(values, data.area.values, 5, alpha=0.7)
            plt.xlabel(name)
            plt.ylabel("area")
            plt.autoscale()
            plt.title("correlation of {0} vs area".format(name))
            plt.show()
            #plt.savefig("corr/correlation of {0} vs area".format(name))
            plt.clf()


"""This block of code is wriiten to provide data frames across code"""
data = pd.read_csv('forestfires.csv')


def correlation_matrix(data):
    plt.matshow(data.corr())
    plt.savefig("correlation matrix", dpi=200)
    plt.clf()


#correlation_matrix(data)
onfires = data[data.area > 0]
onfires_removed_outliers = onfires[onfires.area < 250]
plots_vs_area(onfires_removed_outliers)
bar_plots(onfires_removed_outliers)
#histograms(data)
#histograms(onfires)#for identifying outliers

