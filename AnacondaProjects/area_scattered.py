import pandas as pd
from matplotlib import pyplot as plt
import numpy as np

park_height = 14.5
park_width = 76.7
park_area = park_width * park_height  # in square km and data.area in ha
areas = [[0 for x in range(10)] for y in range(10)]
data = pd.read_csv('forestfires.csv')
on_fires = data[data.area > 0]
print(areas)
xyarea = np.array(on_fires[['X', 'Y', 'area']])
for item in xyarea:
    areas[int(item[0])][int(item[1])]+=item[2]
print(areas)
for item in  xyarea:
    plt.scatter(int(item[0]), int(item[1]), (areas[int(item[0])][int(item[1])]/ park_area)*100) 
    # is unit conversion
plt.xlabel("x location")
plt.ylabel("y location")
plt.title("area cluster identification for max fires")
plt.show()
