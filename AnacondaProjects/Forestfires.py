from PyQt5 import QtCore, QtGui, QtWidgets
import Forestfires_controller


class Ui_ForestFires(object):
    def setupUi(self, ForestFires):
        ForestFires.setObjectName("ForestFires")
        ForestFires.resize(400, 420)
        self.centralWidget = QtWidgets.QWidget(ForestFires)
        self.centralWidget.setObjectName("centralWidget")
        self.gridLayoutWidget = QtWidgets.QWidget(self.centralWidget)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(50, 60, 291, 180))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.gridLayoutWidget)
        self.formLayout.setLabelAlignment(
            QtCore.Qt.AlignJustify | QtCore.Qt.AlignTop)
        self.formLayout.setFormAlignment(
            QtCore.Qt.AlignJustify | QtCore.Qt.AlignTop)
        self.formLayout.setContentsMargins(11, 11, 11, 11)
        self.formLayout.setSpacing(6)
        self.formLayout.setObjectName("formLayout")
        self.tx_month = QtWidgets.QLabel(self.gridLayoutWidget)
        self.tx_month.setObjectName("tx_month")
        self.formLayout.setWidget(
            1, QtWidgets.QFormLayout.LabelRole, self.tx_month)
        self.cb_month = QtWidgets.QComboBox(self.gridLayoutWidget)
        self.cb_month.setEditable(False)
        self.cb_month.setObjectName("cb_month")
        self.formLayout.setWidget(
            1, QtWidgets.QFormLayout.FieldRole, self.cb_month)
        self.tx_ffmc = QtWidgets.QLabel(self.gridLayoutWidget)
        self.tx_ffmc.setObjectName("tx_ffmc")
        self.formLayout.setWidget(
            2, QtWidgets.QFormLayout.LabelRole, self.tx_ffmc)
        self.lb_ffmc = QtWidgets.QLineEdit(self.gridLayoutWidget)
        self.lb_ffmc.setObjectName("lb_ffmc")
        self.formLayout.setWidget(
            2, QtWidgets.QFormLayout.FieldRole, self.lb_ffmc)
        self.tx_temp = QtWidgets.QLabel(self.gridLayoutWidget)
        self.tx_temp.setObjectName("tx_temp")
        self.formLayout.setWidget(
            3, QtWidgets.QFormLayout.LabelRole, self.tx_temp)
        self.lb_temp = QtWidgets.QLineEdit(self.gridLayoutWidget)
        self.lb_temp.setObjectName("lb_temp")
        self.formLayout.setWidget(
            3, QtWidgets.QFormLayout.FieldRole, self.lb_temp)
        self.tx_rh = QtWidgets.QLabel(self.gridLayoutWidget)
        self.tx_rh.setObjectName("tx_rh")
        self.formLayout.setWidget(
            4, QtWidgets.QFormLayout.LabelRole, self.tx_rh)
        self.lb_rh = QtWidgets.QLineEdit(self.gridLayoutWidget)
        self.lb_rh.setObjectName("lb_rh")
        self.formLayout.setWidget(
            4, QtWidgets.QFormLayout.FieldRole, self.lb_rh)
        self.tx_wind = QtWidgets.QLabel(self.gridLayoutWidget)
        self.tx_wind.setObjectName("tx_wind")
        self.formLayout.setWidget(
            5, QtWidgets.QFormLayout.LabelRole, self.tx_wind)
        self.lb_wind = QtWidgets.QLineEdit(self.gridLayoutWidget)
        self.lb_wind.setObjectName("lb_wind")
        self.formLayout.setWidget(
            5, QtWidgets.QFormLayout.FieldRole, self.lb_wind)
        self.tx_rain = QtWidgets.QLabel(self.gridLayoutWidget)
        self.tx_rain.setObjectName("tx_rain")
        self.formLayout.setWidget(
            7, QtWidgets.QFormLayout.LabelRole, self.tx_rain)
        self.lb_rain = QtWidgets.QLineEdit(self.gridLayoutWidget)
        self.lb_rain.setObjectName("lb_rain")
        self.formLayout.setWidget(
            7, QtWidgets.QFormLayout.FieldRole, self.lb_rain)
        self.tx_algo = QtWidgets.QLabel(self.gridLayoutWidget)
        self.tx_algo.setObjectName("tx_algo")
        self.formLayout.setWidget(
            8, QtWidgets.QFormLayout.LabelRole, self.tx_algo)
        self.cb_algo = QtWidgets.QComboBox(self.gridLayoutWidget)
        self.cb_algo.setObjectName("cb_algo")
        self.formLayout.setWidget(
            8, QtWidgets.QFormLayout.FieldRole, self.cb_algo)
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.centralWidget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(50, 250, 291, 80))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(
            self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btn_submit = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.btn_submit.setObjectName("btn_submit")
        self.horizontalLayout.addWidget(self.btn_submit)
        self.btn_abt = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.btn_abt.setObjectName("btn_abt")
        self.horizontalLayout.addWidget(self.btn_abt)
        self.title = QtWidgets.QLabel(self.centralWidget)
        self.title.setGeometry(QtCore.QRect(84, 0, 231, 41))
        self.title.setMaximumSize(QtCore.QSize(16777215, 41))
        font = QtGui.QFont()
        font.setFamily("URW Bookman L")
        font.setPointSize(17)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.title.setFont(font)
        self.title.setObjectName("title")
        ForestFires.setCentralWidget(self.centralWidget)
        self.menuBar = QtWidgets.QMenuBar(ForestFires)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 400, 22))
        self.menuBar.setObjectName("menuBar")
        ForestFires.setMenuBar(self.menuBar)
        self.mainToolBar = QtWidgets.QToolBar(ForestFires)
        self.mainToolBar.setObjectName("mainToolBar")
        ForestFires.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QtWidgets.QStatusBar(ForestFires)
        self.statusBar.setObjectName("statusBar")
        ForestFires.setStatusBar(self.statusBar)
        month_list = ["April", "August", "December", "February", "January",
                      "July", "June", "March", "May", "November", "October", "September"]
        self.cb_month.addItems(month_list)
        algo_list = ["BAyesian Ridge",
                     "SVR", "Gaussian SVM"]
        self.cb_algo.addItems(algo_list)
        self.btn_submit.clicked.connect(self.submit)
        self.btn_abt.clicked.connect(self.about)
        self.msg = QtWidgets.QMessageBox()
        self.retranslateUi(ForestFires)
        QtCore.QMetaObject.connectSlotsByName(ForestFires)

    def retranslateUi(self, ForestFires):
        _translate = QtCore.QCoreApplication.translate
        ForestFires.setWindowTitle(_translate("ForestFires", "ForestFires"))
        self.tx_month.setText(_translate("ForestFires", "Month"))
        self.tx_ffmc.setText(_translate("ForestFires", "FFMC"))
        self.tx_temp.setText(_translate("ForestFires", "Temperature  "))
        self.tx_rh.setText(_translate("ForestFires", "Rel. Humidity  "))
        self.tx_wind.setText(_translate("ForestFires", "Wind"))
        self.tx_rain.setText(_translate("ForestFires", "Rain (in cm)"))
        self.tx_algo.setText(_translate("ForestFires", "Algorithm"))
        self.btn_submit.setText(_translate("ForestFires", "Submit"))
        self.btn_abt.setText(_translate("ForestFires", "About"))
        self.title.setText(_translate("ForestFires", "FOREST FIRES"))

    def submit(self):
        ls = []
        try:
            ls.append(self.cb_month.currentIndex())
            ls.append(float(self.lb_ffmc.text()))
            ls.append(float(self.lb_temp.text()))
            ls.append(float(self.lb_rh.text()))
            ls.append(float(self.lb_wind.text()))
            ls.append(float(self.lb_rain.text()))
            res = Forestfires_controller.prediction(
                self.cb_algo.currentIndex(), ls)
            self.msg.setIcon(QtWidgets.QMessageBox.Information)
            self.msg.setWindowTitle("RESULT")
            self.msg.setText(str(res))
            self.msg.show()
        except:
            self.msg.setIcon(QtWidgets.QMessageBox.Critical)
            self.msg.setWindowTitle("Error")
            self.msg.setText("Enter Proper values")
            self.msg.show()

    def about(self):
        self.msg.setIcon(QtWidgets.QMessageBox.Information)
        self.msg.setWindowTitle("About")
        self.msg.setText(Forestfires_controller.about_string(
            self.cb_algo.currentIndex()))
        #msg.button(QtWidgets.QMessageBox.Ok)
        self.msg.show()
        


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ForestFires = QtWidgets.QMainWindow()
    ui = Ui_ForestFires()
    ui.setupUi(ForestFires)
    ForestFires.show()
    sys.exit(app.exec_())
