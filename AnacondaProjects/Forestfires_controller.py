import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import BayesianRidge
from sklearn import metrics
from sklearn.svm import SVR
from sklearn.preprocessing import LabelEncoder
from sklearn import gaussian_process
import numpy as np

data = pd.read_csv("C:/Users/srina/Mini project/AnacondaProjects/clean_data.csv")
data = data.drop('X', axis=1)
data = data.drop('Y', axis=1)
data = data.drop('ISI', axis=1)
data = data.drop('DMC', axis=1)
data = data.drop('DC', axis=1)

label_encoder = LabelEncoder()
data.month = label_encoder.fit_transform(data.month)

data_y = data.area
data = data.drop('area', axis=1)
train_x, test_x, train_y,  test_y = train_test_split(
    data, data_y, test_size=0.2)

algos = [ BayesianRidge(), SVR(),
         gaussian_process.GaussianProcessRegressor()]

def prediction(index,ls):
    regressor = algos[index]
    regressor.fit(data, data_y)
    obtained_y = regressor.predict(np.array(ls).reshape(1,-1))
    return str(obtained_y)

def about_string(index):
    regressor = algos[index]
    regressor.fit(data, data_y)
    obtained_y = regressor.predict(test_x)
    return "The Selected Algorithm is {0} \n Accuracy: {1}  \n R-squared error : {2}".format(str(algos[index]), metrics.r2_score(test_y, obtained_y), metrics.mean_squared_error(test_y, obtained_y))

#print(about_string(1))
#print(prediction(2, [ 7,  91.7, 8.3, 97, 4,0.2]))
