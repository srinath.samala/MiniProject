import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import BayesianRidge
from sklearn import metrics
from sklearn.svm import SVR
from sklearn.preprocessing import LabelEncoder
from sklearn import gaussian_process


data = pd.read_csv('clean_data.csv')
data=data.drop('X', axis=1)
data=data.drop('Y', axis=1)
data=data.drop('ISI', axis=1)
data=data.drop('DMC', axis=1)
data=data.drop('DC', axis=1)

label_encoder = LabelEncoder()
data.month = label_encoder.fit_transform(data.month)

data_y = data.area
data = data.drop('area',axis=1)
train_x, test_x, train_y,  test_y = train_test_split(data, data_y, test_size=0.2)
print(data.head())
#print("training x")
#print(train_x.head())
#print("training y")
#print(train_y.head())

print("test and train svr")
regressor = SVR()
regressor.fit(train_x, train_y)
obtained_y = regressor.predict(test_x)
print(metrics.mean_squared_error(test_y, obtained_y))
print(metrics.r2_score(test_y, obtained_y))

print("bayesian ridge")
regressor =BayesianRidge()
regressor.fit(train_x, train_y)
obtained_y=regressor.predict(train_x)
print(metrics.mean_squared_error(train_y, obtained_y))
print(metrics.r2_score(train_y, obtained_y))

print("no split svr")
regressor = SVR()
regressor.fit(data, data_y)
obtained_y=regressor.predict(test_x)
print(metrics.mean_squared_error(test_y, obtained_y))
print(metrics.r2_score(test_y, obtained_y))

'''
the reason to use a single set of regression is 
that any regression algorithm expects a normal distribution of 
Variate presented to it.
'''
print("no split gaussian svm")
regressor =gaussian_process.GaussianProcessRegressor()
regressor.fit(data, data_y)
obtained_y=regressor.predict(test_x)
print(metrics.mean_squared_error(test_y, obtained_y))
print(metrics.r2_score(test_y, obtained_y))


print("test and train gaussian svm")
regressor =gaussian_process.GaussianProcessRegressor()
regressor.fit(train_x, train_y)
obtained_y=regressor.predict(test_x)
print(metrics.mean_squared_error(test_y, obtained_y))
print(metrics.r2_score(test_y, obtained_y))

#data=data['month','temp','RH','wind','rain','area']
